<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="FI Generator-Favicon Generator for every browser and any platform">
    <meta name="keywords" content="favicon, favicon generator, generate favicon, fi generator">
    <title>@yield('title') | FI Generator</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Compiled CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Generic -->
    <link rel="icon" href="{{ asset('images/favicons/favicon-32.png') }}" sizes="32x32">
    <link rel="icon" href="{{ asset('images/favicons/favicon-57.png') }}" sizes="57x57">
    <link rel="icon" href="{{ asset('images/favicons/favicon-76.png') }}" sizes="76x76">
    <link rel="icon" href="{{ asset('images/favicons/favicon-96.png') }}" sizes="96x96">
    <link rel="icon" href="{{ asset('images/favicons/favicon-128.png') }}" sizes="128x128">
    <link rel="icon" href="{{ asset('images/favicons/favicon-192.png') }}" sizes="192x192">
    <link rel="icon" href="{{ asset('images/favicons/favicon-228.png') }}" sizes="228x228">

    <!-- Android -->
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('images/favicons/favicon-196.png') }}">

    <!-- Web App Manifest -->
    <link rel="manifest" href="{{ asset('images/favicons/site.webmanifest') }}">

    <!-- Windows 8 IE 10 -->
    <meta name="msapplication-TileColor" content="#A61D5A">
    <meta name="msapplication-TileImage" content="{{ asset('images/favicons/favicon-144.png') }}">

    <!-- Windows 8.1 + IE11 and above -->
    <meta name="msapplication-config" content="{{ asset('images/favicons/browserconfig.xml') }}">

    <!-- iOS -->
    <link rel="apple-touch-icon" href="{{ asset('images/favicons/favicon-120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ asset('images/favicons/favicon-152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ asset('images/favicons/favicon-180.png') }}" sizes="180x180">
</head>
<body>
<div class="wrapper" id="app">
    <header>
        <!-- Image and text -->
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img src="{{ asset('images/fi-logo.png') }}" width="30" height="30" class="d-inline-block align-top" alt="FIG Logotype">
                Fi Generator
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item @if(request()->segment(1) == '') active @endif">
                        <a class="nav-link" href="{{ route('index') }}">Home @if(request()->segment(1) == '')<span class="sr-only">(current)</span>@endif</a>
                    </li>
                    <li class="nav-item @if(request()->segment(1) == 'examples') active @endif">
                        <a class="nav-link" href="{{ route('examples') }}">Examples @if(request()->segment(1) == 'examples')<span class="sr-only">(current)</span>@endif</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <section class="pb-5">
        @yield('content')
        <hr>
    </section>
    <footer class="footer bg-light text-center">
        <p>Fi Generator &copy; 2019 - {{ date('Y') }}<br>
            Developed and Maintained by <a class="btn btn-link pl-0" href="https://nruslan.com" data-toggle="tooltip" data-placement="top" title="Ruslan Niyazimbetov" target="_blank">Ruslan</a></p>
    </footer>
</div>
<!-- Compiled JS -->
<script src="{{ asset('js/app.js') }}"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
@yield('script')
</body>
</html>
