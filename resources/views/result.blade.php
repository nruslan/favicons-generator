@extends('layouts.app')

@section('title', 'Generated Favicons Files')

@section('content')
    <h1 class="text-center m-3">Place the Following in the &lt;head&gt;&lt;/head&gt; of your HTML</h1>
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-6">
                <h3><i class="fas fa-globe"></i> For Any Website</h3>

<pre>
<code class="html">
@foreach($types as $type)
&lt;!-- {{ $type->name }} --&gt;
@if($type->id == 1)
@foreach($type->sizes as $size)
&lt;link rel=&quot;icon&quot; href=&quot;{{ $path }}/favicons/{{ $size->name }}{{ $size->width }}.png&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot;&gt;
@endforeach

@elseif($type->id == 2)
@foreach($type->sizes as $size)
&lt;link rel=&quot;shortcut icon&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot; href=&quot;{{ $path }}/favicons/{{ $size->name }}{{ $size->width }}.png&quot;&gt;
@endforeach

@elseif($type->id == 3)
&lt;link rel=&quot;manifest&quot; href=&quot;{{ $path }}/favicons/site.webmanifest&quot;&gt;

@elseif($type->id == 4)
@foreach($type->sizes as $size)
&lt;meta name=&quot;msapplication-TileColor&quot; content=&quot;{{ $hexCode }}&quot;&gt;
&lt;meta name=&quot;msapplication-TileImage&quot; content=&quot;{{ $path }}/favicons/{{ $size->name }}{{ $size->width }}.png&quot;&gt;
@endforeach

@elseif($type->id == 5)
&lt;meta name=&quot;msapplication-config&quot; content=&quot;{{ $path }}/favicons/browserconfig.xml&quot;&gt;

@elseif($type->id == 6)
@foreach($type->sizes as $size)
&lt;link rel=&quot;apple-touch-icon&quot; href=&quot;{{ $path }}/favicons/{{ $size->name }}{{ $size->width }}.png&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot;&gt;
@endforeach
@endif
@endforeach

</code>
</pre>
            </div><!-- /Col -->
            <div class="col-12 col-md-6">
                <h3><i class="fab fa-laravel"></i> For Laravel Artisans</h3>
<pre>
<code class="html">
@foreach($types as $type)
&lt;!-- {{ $type->name }} --&gt;
@if($type->id == 1)
@foreach($type->sizes as $size)
&lt;link rel=&quot;icon&quot; href=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/{{ $size->fi_name }}&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot; sizes=&quot;{{ $size->fi_size }}&quot;&gt;
@endforeach

@elseif($type->id == 2)
@foreach($type->sizes as $size)
&lt;link rel=&quot;shortcut icon&quot; sizes=&quot;{{ $size->fi_size }}&quot; href=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/{{ $size->fi_name }}&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot;&gt;
@endforeach

@elseif($type->id == 3)
&lt;link rel=&quot;manifest&quot; href=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/site.webmanifest&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot;&gt;

@elseif($type->id == 4)
@foreach($type->sizes as $size)
&lt;meta name=&quot;msapplication-TileColor&quot; content=&quot;{{ $hexCode }}&quot;&gt;
&lt;meta name=&quot;msapplication-TileImage&quot; content=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/{{ $size->fi_name }}&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot;&gt;
@endforeach

@elseif($type->id == 5)
&lt;meta name=&quot;msapplication-config&quot; content=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/browserconfig.xml&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot;&gt;

@elseif($type->id == 6)
@foreach($type->sizes as $size)
&lt;link rel=&quot;apple-touch-icon&quot; href=&quot;&lcub;&lcub; asset&lpar;&apos;favicons/{{ $size->fi_name }}&apos;&rpar; <span class="token punctuation">&rcub;</span><span class="token punctuation">&rcub;</span>&quot; sizes=&quot;{{ $size->fi_size }}&quot;&gt;
@endforeach
@endif
@endforeach
</code>
</pre>

            </div><!-- /Col -->
        </div><!-- /Row -->
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{ route('download', ['file' => $zipFile]) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Download generated files" target="_blank">Download <i class="fas fa-fw fa-download"></i></a>
            </div>
        </div>
    </div><!-- /Container -->
@endsection

@section('script')
    <script>hljs.initHighlightingOnLoad();</script>
@endsection