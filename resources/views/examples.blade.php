@extends('layouts.app')

@section('title', 'Examples')

@section('content')
    <div class="container-fluid">
        <h1 class="text-center mt-5">Code and Files Example</h1>
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-6 col-lg-6">
                <h2 class="text-center mt-5">Generated Code Example</h2>
                <pre>
<code class="html">
@foreach($types as $type)
&lt;!-- {{ $type->name }} --&gt;
@if($type->id == 1)
@foreach($type->sizes as $size)
&lt;link rel=&quot;icon&quot; href=&quot;/favicons/{{ $size->name }}{{ $size->width }}.png&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot;&gt;
@endforeach

@elseif($type->id == 2)
@foreach($type->sizes as $size)
&lt;link rel=&quot;shortcut icon&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot; href=&quot;/favicons/{{ $size->name }}{{ $size->width }}.png&quot;&gt;
@endforeach

@elseif($type->id == 3)
&lt;link rel=&quot;manifest&quot; href=&quot;/favicons/site.webmanifest&quot;&gt;

@elseif($type->id == 4)
@foreach($type->sizes as $size)
&lt;meta name=&quot;msapplication-TileColor&quot; content=&quot;{{ $hexCode }}&quot;&gt;
&lt;meta name=&quot;msapplication-TileImage&quot; content=&quot;/favicons/{{ $size->name }}{{ $size->width }}.png&quot;&gt;
@endforeach

@elseif($type->id == 5)
&lt;meta name=&quot;msapplication-config&quot; content=&quot;/favicons/browserconfig.xml&quot;&gt;

@elseif($type->id == 6)
@foreach($type->sizes as $size)
&lt;link rel=&quot;apple-touch-icon&quot; href=&quot;/favicons/{{ $size->name }}{{ $size->width }}.png&quot; sizes=&quot;{{ $size->width }}x{{ $size->height }}&quot;&gt;
@endforeach
@endif
@endforeach

</code>
</pre>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                @foreach($types as $type)
                    <h2 class="text-center mt-5">{{ $type->name }}</h2>
                    <div class="row">
                        @foreach($type->sizes as $size)
                            <div class="col">
                                <img class="img-thumbnail" src="https://via.placeholder.com/{{ $size->width }}x{{ $size->height }}" alt="" width="{{ $size->width }}">
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>hljs.initHighlightingOnLoad();</script>
@endsection