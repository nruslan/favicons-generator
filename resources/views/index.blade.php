@extends('layouts.app')

@section('title', 'Favicons 2019')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center align-items-center">
            <div class="col-12 col-md-8 col-lg-6">
                <h1 class="text-center mt-5">Fi Generator<br><small>Favorite Icon Generator</small></h1>
                <p class="lead text-center">Favicon Generator for every browser and any platform</p>

                <div class="row justify-content-md-center align-items-center">
                    <div class="col-12 col-md-10 text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Chrome"><i class="fab fa-2x fa-fw fa-chrome"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Internet Explorer"><i class="fab fa-fw fa-2x fa-internet-explorer"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Edge"><i class="fab fa-fw fa-2x fa-edge"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Safari"><i class="fab fa-fw fa-2x fa-safari"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Firefox"><i class="fab fa-fw fa-2x fa-firefox"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Opera"><i class="fab fa-fw fa-2x fa-opera"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Windows"><i class="fab fa-fw fa-2x fa-windows"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Android"><i class="fab fa-fw fa-2x fa-android"></i></li>
                            <li class="list-inline-item" data-toggle="tooltip" data-placement="top" title="Apple"><i class="fab fa-fw fa-2x fa-apple"></i></li>
                        </ul>
                    </div>
                </div>


                <form class="needs-validation" novalidate method="POST" action="{{ route('generate.favicons') }}" id="my-form" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Path to favicons folder</label>
                        <div class="input-group">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="left" title="path to favicons folder">
                                <div class="input-group-text"><i class="fas fa-link"></i></div>
                            </div>
                            <input type="text" name="path" value="{{ old('path') }}" class="form-control{{ $errors->has('path') ? ' is-invalid' : '' }}" id="path" aria-describedby="pathHelp" placeholder="http://www.website.com/images" required>
                            @if ($errors->has('path'))<div class="invalid-feedback" role="alert"><strong>{{ $errors->first('path') }}</strong></div>@endif
                        </div>
                        <small id="pathHelp" class="form-text text-muted">Path to the folder "favicons"</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Windows Tile and Web Manifest background color</label>
                        <div class="input-group cp">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="left" title="hex code background color">
                                <div class="input-group-text"><i class="fas fa-palette"></i></div>
                            </div>
                            <input name="hex_code" value="{{ old('hex_code') ?? 'A61D5A' }}" class="jscolor form-control{{ $errors->has('hex_code') ? ' is-invalid' : '' }}" id="hex_code" aria-describedby="hex_codeHelp" placeholder="Hex Code" required>
                            @if ($errors->has('hex_code'))<div class="invalid-feedback" role="alert"><strong>{{ $errors->first('hex_code') }}</strong></div>@endif
                        </div>

                        <small id="hex_codeHelp" class="form-text text-muted">Hex Color Code</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="image_file" class="custom-file-input{{ $errors->has('image_file') ? ' is-invalid' : '' }}" id="file" required>
                            <label class="custom-file-label" for="validatedCustomFile">PNG Image (512 x 512 or larger)</label>
                            @if ($errors->has('image_file'))<div class="invalid-feedback" role="alert"><strong>{{ $errors->first('image_file') }}</strong></div>@endif
                        </div>
                        <small id="fileHelp" class="form-text text-muted">max image size 500 kb (0.5 mb)</small>
                    </div>

                    <div class="text-center">
                        <button type="submit" id="submit-button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="click to generate">Generate <i class="fas fa-cog fa-fw"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center mt-5">
            <div class="col-12 col-md-8">
                <h2>What is a Favicon?</h2>
                <p>A favicon (short for favorite icon), also known as a shortcut icon, website icon, tab icon, URL icon, or bookmark icon, is a file containing one or more small icons, associated with a particular website or web page.
                    <a href="https://en.wikipedia.org/wiki/Favicon" target="_blank">Wikipedia</a></p>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("input[type=file]").change(function(event){
            let fileName = event.target.files[0].name;
            if (event.target.nextElementSibling!=null){
                event.target.nextElementSibling.innerText=fileName;
            }
        });

        (function (){
            'use strict';
            let myForm = document.getElementById('my-form');
            let submitBtn = document.getElementById('submit-button');
            myForm.addEventListener("submit", e => {
                submitBtn.removeChild(submitBtn.lastChild);
                submitBtn.insertAdjacentHTML('beforeend', '<i class="fas fa-cog fa-spin fa-fw"></i>');
                submitBtn.setAttribute("disabled", "true");
            });
        })();
    </script>
@endsection
