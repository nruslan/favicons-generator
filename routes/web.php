<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('/examples', 'IndexController@examples')->name('examples');
Route::post('/generate', 'IndexController@generate')->name('generate.favicons');
Route::get('/result', 'IndexController@result')->name('result');
Route::get('/download/{file}', 'IndexController@download')->name('download');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
