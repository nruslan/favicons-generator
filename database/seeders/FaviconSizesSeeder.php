<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FaviconSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'favicon-', 'width' => 32, 'height' => 32],
            ['id' => 2, 'name' => 'favicon-', 'width' => 57, 'height' => 57],
            ['id' => 3, 'name' => 'favicon-', 'width' => 70, 'height' => 70],
            ['id' => 4, 'name' => 'favicon-', 'width' => 76, 'height' => 76],
            ['id' => 5, 'name' => 'favicon-', 'width' => 96, 'height' => 96],
            ['id' => 6, 'name' => 'favicon-', 'width' => 120, 'height' => 120],
            ['id' => 7, 'name' => 'favicon-', 'width' => 128, 'height' => 128],
            ['id' => 8, 'name' => 'favicon-', 'width' => 144, 'height' => 144],
            ['id' => 9, 'name' => 'favicon-', 'width' => 150, 'height' => 150],
            ['id' => 10, 'name' => 'favicon-', 'width' => 152, 'height' => 152],
            ['id' => 11, 'name' => 'favicon-', 'width' => 180, 'height' => 180],
            ['id' => 12, 'name' => 'favicon-', 'width' => 192, 'height' => 192],
            ['id' => 13, 'name' => 'favicon-', 'width' => 196, 'height' => 196],
            ['id' => 14, 'name' => 'favicon-', 'width' => 228, 'height' => 228],
            ['id' => 15, 'name' => 'favicon-', 'width' => 512, 'height' => 512]
        ];


        $type_sizes = [
            ['id' => 1, 'favicon_type_id' => 1, 'favicon_size_id' => 1, 'description' => 'Standard for most desktop browsers'],
            ['id' => 2, 'favicon_type_id' => 1, 'favicon_size_id' => 2, 'description' => 'Standard iOS home screen (iPod Touch, iPhone first generation to 3G)'],
            ['id' => 3, 'favicon_type_id' => 5, 'favicon_size_id' => 3, 'description' => 'IE11 and above'],
            ['id' => 4, 'favicon_type_id' => 1, 'favicon_size_id' => 4, 'description' => 'iPad home screen icon'],
            ['id' => 5, 'favicon_type_id' => 1, 'favicon_size_id' => 5, 'description' => 'GoogleTV icon'],
            ['id' => 6, 'favicon_type_id' => 6, 'favicon_size_id' => 6, 'description' => 'iPhone retina touch icon'],
            ['id' => 7, 'favicon_type_id' => 1, 'favicon_size_id' => 7, 'description' => 'Chrome Web Store icon & Small Windows 8 Star Screen Icon*'],
            ['id' => 8, 'favicon_type_id' => 4, 'favicon_size_id' => 8, 'description' => 'IE10 Metro tile for pinned site*'],
            ['id' => 9, 'favicon_type_id' => 5, 'favicon_size_id' => 9, 'description' => 'IE11 and above'],
            ['id' => 10, 'favicon_type_id' => 6, 'favicon_size_id' => 10, 'description' => 'iPad touch icon'],
            ['id' => 11, 'favicon_type_id' => 6, 'favicon_size_id' => 11, 'description' => 'iPhone 6 plus'],
            ['id' => 12, 'favicon_type_id' => 1, 'favicon_size_id' => 12, 'description' => NULL],
            ['id' => 13, 'favicon_type_id' => 2, 'favicon_size_id' => 13, 'description' => 'Chrome for Android home screen icon'],
            ['id' => 14, 'favicon_type_id' => 1, 'favicon_size_id' => 14, 'description' => 'Opera Coast icon'],
            ['id' => 15, 'favicon_type_id' => 3, 'favicon_size_id' => 15, 'description' => 'Google Developer Web App Manifest Recommendation'],
            ['id' => 16, 'favicon_type_id' => 3, 'favicon_size_id' => 16, 'description' => 'For Android Chrome M47+ Splash screen with 1.5 screen density.']
        ];

        foreach($items as $item) {
            \App\FaviconSize::updateOrCreate(['id' => $item['id']], $item);

            $size = \App\FaviconSize::find($item['id']);

            foreach($type_sizes as $ts) {
                if ($ts['favicon_size_id'] == $item['id'])
                    $size->types()->attach($ts['favicon_type_id'], ['description' => $ts['description']]);
            }
            
        }

        
    }
}
