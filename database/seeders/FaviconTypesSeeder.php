<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FaviconTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Generic', 'description' => NULL],
            ['id' => 2, 'name' => 'Android', 'description' => NULL],
            ['id' => 3, 'name' => 'Web App Manifest', 'description' => 'The web app manifest is a simple JSON file that tells the browser about your web application and how it should behave when \'installed\' on the user\'s mobile device or desktop.'],
            ['id' => 4, 'name' => 'Windows 8 IE 10', 'description' => NULL],
            ['id' => 5, 'name' => 'Windows 8.1 + IE11 and above', 'description' => NULL],
            ['id' => 6, 'name' => 'iOS', 'description' => NULL,]
        ];

        foreach($items as $item) {
            \App\FaviconType::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
