<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetTypeSizesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_sizes', function (Blueprint $table) {
            $table->foreign('favicon_type_id')->references('id')->on('favicon_types')->onDelete('cascade');
            $table->foreign('favicon_size_id')->references('id')->on('favicon_sizes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_sizes', function (Blueprint $table) {
            $table->dropForeign('type_sizes_favicon_type_id_foreign');
            $table->dropForeign('type_sizes_favicon_size_id_foreign');
        });
    }
}
