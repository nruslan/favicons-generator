<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_sizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('favicon_type_id', false, true)->nullable();
            $table->integer('favicon_size_id', false, true)->nullable();
            $table->text('description')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_sizes');
    }
}
