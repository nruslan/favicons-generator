<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaviconSize extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'width', 'height',
    ];

    /**
     * Get the sizes for the type.
     */
    public function types()
    {
        return $this->belongsToMany(FaviconType::class, 'type_sizes', 'favicon_size_id', 'favicon_type_id');
    }

    public function getFiNameAttribute()
    {
        return $this->name.$this->width.'.png';
    }

    public function getFiSizeAttribute()
    {
        return $this->width.'x'.$this->height;
    }
}
