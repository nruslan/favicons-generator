<?php

namespace App\Http\Controllers;

use App\FaviconSize;
use App\FaviconType;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view("index");
    }

    public function examples()
    {
        $types = FaviconType::all();
        $sizes = FaviconSize::all();
        $hexCode = "#FFFFFF";
        return view("examples", compact('sizes', 'types', 'hexCode'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generate(Request $request)
    {
        // Validate incoming data
        $request->validate([
            'path' => 'required',
            'hex_code' => 'required',
            'image_file' => 'required|max:500|mimes:png'
        ]);
        // Get uploaded image extension
        $fileExtension = $request->file('image_file')->extension();
        // All available sizes
        $sizes = FaviconSize::all();
        // Favicon folder name
        $faviconsFolder = uniqid();//'favicons';
        // Path to the folder
        $pathToFolder = "app/public/$faviconsFolder";
        //Background color
        $bgColor = "#$request->hex_code";

        // Loop through sizes
        foreach($sizes as $size){
            // file name with extension
            $filename = $size->name.$size->width.".$fileExtension";
            // Store uploaded file and return path to the file
            $path = $request->file('image_file')->storeAs("public/$faviconsFolder", "$filename");

            // Get storage path
            $storagePath = storage_path("app/$path");
            //Resize stored image
            $newImage = \Image::make($storagePath)->resize($size->width, $size->height);
            // and save file
            $newImage->save($storagePath, 100);
        }

        // Create XML document for IE browsers
        $this->createXml($pathToFolder, "$bgColor");

        //Create webmanifest file
        $this->createWebmanifest($pathToFolder,"$bgColor");

        $dir = storage_path($pathToFolder);
        $destination = base_path("storage/$pathToFolder.zip");
        //Create Archive with the files
        $this->zipData($dir, $destination);
        // Delete files and folder except zip archive
        $this->deleteAll($dir, $faviconsFolder);

        return redirect(route('result'))
            ->with([
                'path' => $request->path,
                'hexCode' => $bgColor,
                'zipfile' => "$faviconsFolder.zip"
            ]);
    }

    public function result()
    {
        $path = request()->session()->get('path');
        $zipFile = request()->session()->get('zipfile');
        $hexCode = request()->session()->get('hexCode');

        if($path && $zipFile && $hexCode) {
            $types = FaviconType::all();
            $sizes = FaviconSize::all();
            return view("result", compact('sizes', 'types', 'path', 'hexCode', 'zipFile'));
        } else {
            return redirect(route('index'));
        }

        /*$types = FaviconType::all();
        $sizes = FaviconSize::all();
        return view("result", compact('sizes', 'types', 'path', 'hexCode', 'zipFile'));*/
    }

    public function download($file)
    {
        return Storage::download("public/$file", "favicons.zip");
    }

    public static function createXml(string $path, $hexCode)
    {
        $dom = new \DOMDocument();

        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = true;

        $xml_file_name = 'browserconfig.xml';

        $root = $dom->createElement("browserconfig");
        $dom->appendChild($root);

        $msapp_node = $dom->createElement('msapplication');
        $root->appendChild($msapp_node);

        $title_node = $dom->createElement('title');
        $msapp_node->appendChild($title_node);

        $square70_node = $dom->createElement('square70x70logo');
        $square70_node->setAttribute('src', 'favicon-70.png');
        $square150_node = $dom->createElement('square150x150logo');
        $square150_node->setAttribute('src', 'favicon-150.png');

        $title_node->appendChild($square70_node);
        $title_node->appendChild($square150_node);

        $titleColor_node = $dom->createElement('TileColor', $hexCode);
        $title_node->appendChild($titleColor_node);

        $dom->save(storage_path("$path/$xml_file_name"));
    }

    public static function zipData($source, $destination)
    {
        $zipDirectory = 'favicons';

        if (extension_loaded('zip')) {
            if (file_exists($source)) {
                $zip = new \ZipArchive();
                if ($zip->open($destination, \ZIPARCHIVE::CREATE)) {
                    // Add empty directory to the archive
                    $zip->addEmptyDir($zipDirectory);

                    if ($dh = opendir($source)) {
                        while (($file = readdir($dh)) !== false) {

                            if($file != '' && $file != '.' && $file != '..'){
                                $zip->addFile("$source/$file", "$zipDirectory/$file");
                            }
                        }
                        closedir($dh);
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }

    public static function deleteAll($source, $faviconFolder)
    {
        if ($dh = opendir($source)) {
            while (($file = readdir($dh)) !== false) {

                if($file != '' && $file != '.' && $file != '..'){
                    Storage::delete("public/$faviconFolder/$file");
                }
            }
            closedir($dh);
            // Delete directory
            rmdir($source);
        }

        return true;
    }

    public static function createWebmanifest(string $path, string $hexCode)
    {
        // Load manifest sizes
        $type = FaviconType::find(3);
        $file = storage_path("$path/site.webmanifest");
        $current = "{\n";
        $current .= "\t".'"name": "",'."\n";
        $current .= "\t".'"short_name": "",'."\n";
        $current .= "\t".'"icons": ['."\n";
        foreach($type->sizes as $size){
            $current .= "\t\t".'{'."\n";
            $current .= "\t\t\t".'"src": "/'.$size->name.$size->width.'.png",'."\n";
            $current .= "\t\t\t".'"sizes": "'.$size->width.'x'.$size->height.'",'."\n";
            $current .= "\t\t\t".'"type": "image/png"'."\n";
            $current .= "\t\t".'},'."\n";
        }
        $current .= "\t".'],'."\n";
        $current .= "\t".'"theme_color": "'.$hexCode.'",'."\n";
        $current .= "\t".'"background_color": "'.$hexCode.'",'."\n";
        $current .= "\t".'"display": "standalone"'."\n";
        $current .= "}";
        file_put_contents($file, $current, FILE_APPEND | LOCK_EX);
    }
}
