<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaviconType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Get the sizes for the type.
     */
    public function sizes()
    {
        return $this->belongsToMany(FaviconSize::class, 'type_sizes', 'favicon_type_id', 'favicon_size_id');
    }
}
